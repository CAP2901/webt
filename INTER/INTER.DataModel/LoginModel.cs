﻿using INTER.DataAccess;

namespace INTER.DataModel
{
    public class LoginModel
    {
        /// <summary>
        /// variables
        /// </summary>
        public int Id { get; set; }
        
        public string Username { get; set; }
        
        public string Password { get; set; }

        public Role Role { get; set; }

    }
}
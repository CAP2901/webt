﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using INTER.Common.Resources;
using INTER.DataModel;
using Microsoft.AspNetCore.Mvc;
using INTER.Service;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;

using Constants = INTER.Common.Infrastructure.Constants;

namespace INTER.Controllers
{
    public class AccountController : Controller
    {
        #region 
        /// <summary>
        /// 
        /// </summary>
        private readonly IUserService _userService;
        
        /// <summary>
        /// Constructor
        /// </summary>
        public AccountController(IUserService userService)
        {
            _userService = userService;
        }
        #endregion
        /// <summary>
        /// Login httpget
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Login()
        {            
            return View();
        }
        
        /// <summary>
        /// Login httppost
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
       
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (model.Username.Equals(null) || model.Password.Equals(null))
            {
                ModelState.AddModelError(String.Empty, MessageResource.NullUserPass);
           
            }
            else
            if(ModelState.IsValid)
            {
                var user = _userService.Login(model);
                
                if (user == null)
                {
                    ModelState.AddModelError(String.Empty , MessageResource.UserLoginFailed);         
                }
                else
                {
                    var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, user.Username),
                        new Claim(Constants.ClaimName.UserCode, user.Password),
                        new Claim(Constants.ClaimName.AccountId, user.Id.ToString())  
                    };
                    var claimsIdentity = new ClaimsIdentity(
                        claims, CookieAuthenticationDefaults.AuthenticationScheme);
                    // Storage cookies 
                   
                    await HttpContext.SignInAsync(
                        CookieAuthenticationDefaults.AuthenticationScheme, 
                        new ClaimsPrincipal(claimsIdentity), new  AuthenticationProperties
                        {
                            ExpiresUtc =  DateTime.UtcNow.AddMinutes(100) 
                        } );
                    return Redirect("/Admin/Index");
                }

            }

                return View(model);
            
        }
        /// <summary>
        /// LogOut 
        /// </summary>
        /// <returns></returns>
   
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            
            //save system log
            //HttpContext.Session.Remove(Constants.SessionKey);
            Response.Cookies.Delete(Constants.ClaimName.UserName);
            return Redirect("/Account/login");
        }
        
        /// <summary>
        /// show User
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Index()
        {
            var users = _userService.Users();
            return
                View(users);
        }
        

        /// <summary>
        /// View User
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult CreateUser(int id)
        {   
                return View();
        }
        /// <summary>
        /// Create User
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
       [HttpPost]
       [ValidateAntiForgeryToken]
       public IActionResult CreateUser(LoginModel model)
        {
           _userService.CreateUser(model);
           return Redirect("/Account/index");
           
        }
        /// <summary>
        /// Delete User
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IActionResult Delete(int id)
        {
            _userService.Delete(id);
            return Redirect("/Account/index");

        }

        /// <summary>
        ///  show edit
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Edit(int id ,string username)
        {
            _userService.GetbyId(id);
            return View();
        }
        /// <summary>
        /// Update User
        /// </summary>
        /// <returns></returns>
        
        [HttpPost]
        public IActionResult Edit( ChangePassWordModel model)
        {
            _userService.Update(model);
            return  Redirect("/Account/index");
        }

        [HttpGet]
        public IActionResult UpdatePassword()
        {
            return View();
        }
        
        
   

    }
}
﻿using Microsoft.EntityFrameworkCore;

namespace INTER.DataAccess
{
    public class AppDbContext  : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }
        
        public  virtual  DbSet<User> Users { get; set; }
        public  virtual  DbSet<Role> Roles { get; set; }
        public  virtual  DbSet<Classer> Classers { get; set; }
        public  virtual  DbSet<Student> Students { get; set; }
        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<Role>().ToTable("Role");
            modelBuilder.Entity<Classer>().ToTable("Class");
            modelBuilder.Entity<Student>().ToTable("Student"); 

        } 


    }
}
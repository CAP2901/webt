﻿namespace INTER.DataModel
{
    /// <summary>
    /// variables
    /// </summary>
    public class CreateClasserModel
    {
        public int Id { get; set; }
        public string ClassName { get; set; }
        public int ClassMember { get; set; }
    }
}
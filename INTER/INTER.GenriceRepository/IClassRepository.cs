﻿using INTER.DataAccess;

namespace INTER.GenriceRepository
{
    /// <summary>
    /// Class for Repository
    /// </summary>
    public interface IClassRepository : IGenericRepository<Classer>
    {  
    }

    public class ClassRepsitory : GenericRepository<Classer>, IClassRepository
    {
        /// <summary>
        /// class repository
        /// </summary>
        /// <param name="dbContext"></param>
        public ClassRepsitory(AppDbContext dbContext) : base(dbContext)
        {
            
        }
    }
}
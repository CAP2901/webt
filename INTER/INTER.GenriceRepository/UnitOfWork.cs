﻿using INTER.DataAccess;
using System.Threading.Tasks;


namespace INTER.GenriceRepository
{
    public class UnitOfWork : IUnitOfWork
    {
        /// <summary>
        /// Unit of work class
        /// </summary>

        #region Properties
        private readonly AppDbContext _appDbContext;
        private IUserRepostiory _usersRepository;
        private IStudentRepository _studentRepository;
        private IClassRepository _classRepository;
        #endregion
        
        /// <summary>
        /// Unit Of Work Contructor
        /// </summary>
        /// <param name="tapDoorCloudDbContext"></param>
        public UnitOfWork(AppDbContext tapDoorCloudDbContext)
        {
            _appDbContext = tapDoorCloudDbContext;

        }

        #region Method
        /// <summary>
        /// Save
        /// </summary>
        public void Save()
        {
            _appDbContext.SaveChanges();
        }
        /// <summary>
        /// Save Async
        /// </summary>
        /// <returns></returns>
        public async Task SaveAsync()
        {
            await  _appDbContext.SaveChangesAsync();
        }
                
        /// <summary>
        /// Get AppDbContext
        /// </summary> 
        public AppDbContext AppDbContext => _appDbContext;
        public IUserRepostiory UserRepository
        {
            get
            {
                return _usersRepository =
                    _usersRepository ?? new UserRespository(_appDbContext);
            }
        }
        /// <summary>
        /// initalization studentReository
        /// </summary>
        public IStudentRepository StudentRepository
        {
            get
            {
                return _studentRepository =
                    _studentRepository ?? new StudentRepository(_appDbContext);
            }
        }
        /// <summary>
        /// initialization class repository
        /// </summary>
        public IClassRepository ClassRepository
        {
            get { return _classRepository = 
                _classRepository ?? new ClassRepsitory(_appDbContext); }
        }
        #endregion
        
    }
}
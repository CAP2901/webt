﻿using INTER.DataAccess;

namespace INTER.DataModel
{
    public class CreateStudentModel
    {
        public int Id { get; set; }
        public int StudentMasv { get; set; }
        public string StudentName { get; set; }
        public int StudentAge { get; set; }
        public string StudentAddress { get; set; }

        public Classer Classer { get; set; }
    }
}
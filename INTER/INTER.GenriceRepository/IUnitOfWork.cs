﻿using INTER.DataAccess;
using  System.Threading.Tasks;

namespace INTER.GenriceRepository 
{
    /// <summary>
    /// interface for UnitOfWork
    /// </summary>
    public interface IUnitOfWork
    {
        IUserRepostiory UserRepository { get; }
        IStudentRepository StudentRepository { get; }
        IClassRepository ClassRepository { get; }
        AppDbContext AppDbContext { get; }
        void Save();

    }
}
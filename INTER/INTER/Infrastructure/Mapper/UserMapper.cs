﻿using  AutoMapper;
using INTER.DataModel;
using INTER.DataAccess;

namespace INTER.GenriceRepository.Mapper
{
    public class UserMapper
    {
        /// <summary>
        /// create mapping
        /// </summary>
        public class  UserMapping : Profile 
        {
            public UserMapping()
            {
                CreateMap<LoginModel, User>();
                CreateMap<User, LoginModel>();
                CreateMap<ChangePassWordModel, User>();
                CreateMap<CreateClasserModel, Classer>();
                CreateMap<Classer, CreateClasserModel>();
                CreateMap<CreateStudentModel, Classer>();    
                CreateMap<Student, CreateStudentModel>();
            }
         
//            public AccountMapping()
//            {
//                CreateMap<UserDto, User>()
//                    .ForMember(dest => dest.Pass,
//                        opt =>
//                        {
//                            opt.Condition(m => !string.IsNullOrEmpty(m.UserName));
//                            opt.MapFrom(src => Encryptor.CalculateHash(src.UserName));
//                        });
//                CreateMap<ChangePasswordModel, Users>()
//                    .ForMember(dest => dest.UserName, opt => opt.Ignore())
//                    .ForMember(dest => dest.Role, opt => opt.Ignore())
//                    .ForMember(dest => dest.Password,
//                        opt =>
//                        {
//                            opt.Condition(m => !string.IsNullOrEmpty(m.Password));
//                            opt.MapFrom(src => Encryptor.CalculateHash(src.Password));
//                        });
//            }
        }
    }

    
}
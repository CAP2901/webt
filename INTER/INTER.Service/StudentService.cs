﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using INTER.DataAccess;
using INTER.DataModel;
using INTER.GenriceRepository;

namespace INTER.Service
{
    public interface IStudentService
    {        
        IEnumerable<Student> Students();
        Student Student(int id);
        void CreaterStudent(CreateStudentModel model);
        void Delete(int id);
        CreateStudentModel GetById(int id);
        void Update(CreateStudentModel model);
    }

    public class StudentService : IStudentService
    {
        #region  property
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        #endregion
        
        /// <summary>
        ///  UserService Contructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="mapper"></param>
        public StudentService(IUnitOfWork unitOfWork ,IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
       
        public IEnumerable<Student> Students()
        {
            var student = _unitOfWork.StudentRepository.GetAll();
            return student;

        }
        
        public Student Student(int id)
        
        {
            var student = _unitOfWork.StudentRepository.GetAll().FirstOrDefault(s => s.StudentAge.Equals(id));
            return student;

        }
        
        
        public void CreaterStudent(CreateStudentModel model)
        {
            var student = _mapper.Map<CreateStudentModel, Student >(model);
            _unitOfWork.StudentRepository.Add(student);
            _unitOfWork.Save();
        }
        /// <summary>
        /// Delete A Entity of Id 
        /// </summary>
        /// <param name="id"></param>
        public void Delete(int id)
        {
            var student = _unitOfWork.StudentRepository.ObjectContext.FirstOrDefault(s => s.Id.Equals(id));
            _unitOfWork.StudentRepository.Delete(student);
            _unitOfWork.Save();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public  CreateStudentModel GetById(int id)
        {
            var student = _unitOfWork.StudentRepository.GetById(id);
            return _mapper.Map<Student, CreateStudentModel>(student);
        }
        /// <summary>
        /// Update a Entity Class
        /// </summary>
        /// <param name="model"></param>
        public void Update(CreateStudentModel model)
        {
            var classer = _unitOfWork.ClassRepository.ObjectContext.FirstOrDefault(s => s.Id.Equals(model.Id));
            if (classer != null)
            {
             
                _unitOfWork.ClassRepository.Update(classer);
            }
            
            _unitOfWork.Save();
  
        }
    }

}
﻿using System.Collections.Generic;
using System.Linq;
using INTER.DataAccess;
using INTER.Service;
using Microsoft.AspNetCore.Mvc;

namespace INTER.Controllers
{
    public class StudentController : Controller
    {
        private readonly IStudentService _studentService;
        private AppDbContext _appDbContext;

        public StudentController(IStudentService studentService, AppDbContext appDbContext)
        {
            _studentService = studentService;
            _appDbContext = appDbContext;
        }
        /// <summary>
        /// show entity Student 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Show()
        {
            var student = _studentService.Students();
            return
            View(student);
        }

        public IActionResult Create()
        {
//            List<Classer> li = new List<Classer>();
//            li = _studentService
            return View();
        }
        
        public IActionResult Show1(int id)
        {
            var student = _studentService.Students();
            return
                View();
        }
    }
}
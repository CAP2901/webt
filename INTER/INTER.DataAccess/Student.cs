﻿using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace INTER.DataAccess
{
    /// <summary>
    /// Table Student
    /// </summary>
    public class Student
    {
        public int Id { get; set; }
        
        public int StudentMasv { get; set; }
        
        [Column(TypeName = "nvarchar(50)")]
        public string StudentName { get; set; }
         
        public int StudentAge { get; set; }
        
        [Column(TypeName = "nvarchar(50)")]
        public string StudentAddress { get; set; }
        
        /// <summary>
        /// foreignkey class id
        /// </summary>
        [ForeignKey("ClasserId")]
        public Classer Classer { get; set; }
    }
}
﻿﻿using System.ComponentModel.DataAnnotations.Schema;

namespace INTER.DataAccess
{
    public class User
    {
        /// <summary>
        /// Table User
        /// </summary>
        public int Id { get; set; }
        
        [Column(TypeName = "nvarchar(50)")]
        public string Username { get; set; }
        
        [Column(TypeName = "nvarchar(50)")]
        public string Password { get; set; }
        
        /// <summary>
        /// foreign key User
        /// </summary>
        [ForeignKey("RoleId")]
        public Role Role { get; set; }
    }
}
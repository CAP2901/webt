﻿
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using INTER.DataAccess;
using INTER.DataModel;
using INTER.GenriceRepository;

namespace INTER.Service
{
    public interface IClassService
    {
        IEnumerable<Classer> Classers();
        void CreaterClass(CreateClasserModel createrClasser);
        void Delete(int id);
        CreateClasserModel GetById(int id);
        void Update(CreateClasserModel model);
    }
    public class ClasserService :IClassService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ClasserService(IUnitOfWork unitOfWork ,IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        /// <summary>
        /// Show Class member
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Classer> Classers()
        {
            return _unitOfWork.ClassRepository.GetAll();
        }
        
        /// <summary>
        /// Insert a entity Classer
        /// </summary>
        /// <param name="createrClasser"></param>
        public void CreaterClass(CreateClasserModel createrClasser)
        {
            var classer = _mapper.Map<CreateClasserModel, Classer >(createrClasser);
            _unitOfWork.ClassRepository.Add(classer);
            _unitOfWork.Save();
        }
        /// <summary>
        /// Delete A Entity of Id 
        /// </summary>
        /// <param name="id"></param>
        public void Delete(int id)
        {
            var classer = _unitOfWork.ClassRepository.ObjectContext.FirstOrDefault(s => s.Id.Equals(id));
            _unitOfWork.ClassRepository.Delete(classer);
            _unitOfWork.Save();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public  CreateClasserModel GetById(int id)
        {
            var classer = _unitOfWork.ClassRepository.GetById(id);
            return _mapper.Map<Classer, CreateClasserModel>(classer);
        }
        /// <summary>
        /// Update a Entity Class
        /// </summary>
        /// <param name="model"></param>
        public void Update(CreateClasserModel model)
        {
            var classer = _unitOfWork.ClassRepository.ObjectContext.FirstOrDefault(s => s.Id.Equals(model.Id));
            if (classer != null)
            {
                classer.ClassName = model.ClassName;
                classer.ClassMember = model.ClassMember;
                _unitOfWork.ClassRepository.Update(classer);
            }
            
            _unitOfWork.Save();
  
        }
    }
}
﻿using System.Net.Http;
using INTER.DataAccess;
using Microsoft.AspNetCore.Http;

namespace INTER.GenriceRepository
{
    /// <summary>
    /// Interface  for UserRespository
    /// </summary>
    public interface IUserRepostiory :IGenericRepository<User>
    {
        
    }

    public class UserRespository : GenericRepository<User>, IUserRepostiory
    {
        /// <inheritdoc />
        /// <summary>
        /// UserRepository
        /// </summary>
        /// <param name="dbContext"></param>  
        public UserRespository(AppDbContext dbContext) : base(dbContext)
        {
            
        }
    }
}
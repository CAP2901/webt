﻿using INTER.DataAccess;
using Microsoft.AspNetCore.Http;

namespace INTER.GenriceRepository
{
    /// <summary>
    /// interface for StudentRepository
    /// </summary>
    public interface IStudentRepository : IGenericRepository<Student>
    {
        
    }

    public class StudentRepository : GenericRepository<Student>, IStudentRepository
    {
        /// <summary>
        /// StudenRepository
        /// </summary>
        /// <param name="dbContext"></param>
   
        public StudentRepository(AppDbContext dbContext) : base(dbContext)
        {           
        }
    }
    
    
}
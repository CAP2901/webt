﻿using INTER.DataModel;
using INTER.Service;
using Microsoft.AspNetCore.Mvc;

namespace INTER.Controllers
{
    public class ClasserController : Controller
    {

        private readonly IClassService _ClassService;

        public ClasserController(IClassService classService)
        {
            _ClassService = classService;
        }
        /// <summary>
        /// show class
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Show()
        {
            var classer = _ClassService.Classers();
            return
            View(classer);
        }
        /// <summary>
        ///  view create class
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult CreateClass()
        {
            return View();
        }
        
        /// <summary>
        ///  funtion  create class
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult CreateClass(CreateClasserModel createClass)
        {
            _ClassService.CreaterClass(createClass);
            return Redirect("/Classer/Show");
        }
        /// <summary>
        /// delete a Entity Classer for Id 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
       
        public IActionResult DeleteClass(int id)
        {
            _ClassService.Delete(id);
            return Redirect("/Classer/Show");
        }
        
        /// <summary>
        /// view Update
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Update(int id, string name)
        {
            _ClassService.GetById(id);
            return View();
        }
        
        /// <summary>
        ///  Update claseer
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Update(CreateClasserModel model)
        {
            _ClassService.Update(model);
            return Redirect("/Classer/Show");
        }
        
    }
}
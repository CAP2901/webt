﻿using INTER.DataAccess;
using INTER.DataModel;
using System.Collections.Generic;

namespace INTER.Service
{
    public interface IUserService
    {
        /// <summary>
        /// Interface User Service
        /// </summary>
        /// <param name="User"></param>
        /// <returns></returns>
        User Login(LoginModel User);

        void CreateUser(LoginModel user);
        IEnumerable<User> Users();
        void Delete(int id);
       
        void Update(ChangePassWordModel loginModel);
        LoginModel GetbyId(int id);

    }
}
﻿using System.Collections.Generic;
using System.Linq;
using INTER.GenriceRepository;
using INTER.DataAccess;
using AutoMapper;
using INTER.DataModel;

namespace INTER.Service
{
    
    public class UserService :IUserService
    {
        #region  property
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        #endregion
        
        /// <summary>
        ///  UserService Contructor
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="mapper"></param>
        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            
        }
        
        /// <summary>
        /// Check User
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public User Login(LoginModel user)
        {
            User account = _unitOfWork.UserRepository.GetAll()
                .FirstOrDefault(s => s.Username.Equals(user.Username)  && s.Password.Equals( user.Password));
            return account;
        }

       
             
        /// <summary>
        /// Create a User
        /// </summary>
        /// <param name="userDto"></param>
        public void CreateUser(LoginModel userDto)
        {
                var user = _mapper.Map<LoginModel, User>(userDto);
                user.Role = new  Role();
                _unitOfWork.UserRepository.Add(user);
                _unitOfWork.Save();            
        }

        /// <summary>
        /// Show Account
        /// </summary>
        /// <returns></returns>
        public IEnumerable<User> Users()
        {
            return _unitOfWork.UserRepository.GetAll();
        }
               
        /// <summary>
        /// Delete a Account
        /// </summary>
        /// <param name="id"></param>
        public  void Delete(int id)
        {
               User user = _unitOfWork.UserRepository.GetById(id);    
             _unitOfWork.UserRepository.Delete(user);
              _unitOfWork.Save();
            
        }
        
        /// <summary>
        /// GetbyID Account
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>       
        public LoginModel GetbyId(int id)
        {
            var user = _unitOfWork.UserRepository.GetById(id);
            return _mapper.Map<User, LoginModel>(user);
        }
        
        /// <summary>
        /// update a Account
        /// </summary>
        /// <param name="model"></param>
        public void Update(ChangePassWordModel model )
        {    
            
            var user = _unitOfWork.UserRepository.ObjectContext.FirstOrDefault(s => s.Id.Equals(model.Id));
            if (user != null)
            {
                user.Password = model.Password;
                user.Username = model.Username;
                _unitOfWork.UserRepository.Update(user);
            }

            _unitOfWork.Save();

        }

    }
}
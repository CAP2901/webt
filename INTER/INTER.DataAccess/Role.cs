﻿using  System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace INTER.DataAccess
{
    public class Role
    {
        /// <summary>
        /// Table Role
        /// </summary>
        public int Id { get; set; }
            
        [Column(TypeName = "nvarchar(200)")]
        public string Name { get; set; }
                    
        /// <summary>
        /// List user
        /// </summary>
        public ICollection<User> User { get; set; }

    }
}
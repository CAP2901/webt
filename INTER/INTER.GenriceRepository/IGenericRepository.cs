﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace INTER.GenriceRepository
{
    /// <summary>
    /// Generic Repository  interface
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IGenericRepository<T> where T : class
    {
        //Get all<T>
        //Get all entities of type T
        IEnumerable<T> GetAll();
        
        //Get an entity by id
        T GetById(int id, bool alowTracking = true);
        
        // Marks an entity as new
        void Add(T entity);
        
        //Marks an entity to be delete
        void Delete(T entity);
        
        //Marks  an entity as modified
        void Update(T entity);

        Task<IEnumerable<T>> GetAllAsync(bool AllowTracking = true);
        
        // Gets entities using delegate
        Task<T> GetByIdAsync(int id, bool allowTracking = true);
        IQueryable<T> ObjectContext { get; set; }
    }
}
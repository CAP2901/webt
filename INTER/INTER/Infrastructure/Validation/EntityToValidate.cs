﻿using FluentValidation;
using Microsoft.AspNetCore.Identity.UI.V4.Pages.Account.Internal;
using LoginModel = INTER.DataModel.LoginModel;

namespace INTER.Infrastructure.Validation 
{
    public class EntityToValidate : AbstractValidator<LoginModel>
    {
        public EntityToValidate()
        {
            RuleFor(t => t.Username).NotEmpty().Length(1, 10).WithMessage("Please dont more ten charater");
        }
    }
}
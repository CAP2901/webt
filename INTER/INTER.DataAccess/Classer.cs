﻿using System.Collections.Generic;

namespace INTER.DataAccess
{
    public class Classer
    {
        public  int Id { get; set; }
        public  string ClassName { get; set; }
        public  int ClassMember { get; set; } 
        
        public ICollection<Student> Students { get; set; }
    }
}